/* eslint-disable import/no-duplicates */
import * as types from '../types'
declare global {
  interface Window {
    api: {
      send: (channel: string, data: types.WorkParam) => void
      prefabInfo: () => any
      picInfo: () => types.ItemInfoRet
      spineInfo: () => types.ItemInfoRet
      scriptInfo: () => types.ItemInfoRet
    }
  }
}
