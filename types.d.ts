export interface ItemInfo {
    name: string,
    guid: string
}

export interface ItemInfoRet {
    dir: string,
    data: ItemInfo[]
}

export type PrefabRet = { [key: string]: string }

export interface regData {
    spine: string,
    blurimg: string,
    img: string,
    script: string,
}

export type WorkParam = {
    [key: string]: regData
}