const { contextBridge, ipcRenderer } = require("electron")

// Expose protected methods that allow the renderer process to use
// the ipcRenderer without exposing the entire object
contextBridge.exposeInMainWorld("api", {
  send: (channel, data) => {
    // whitelist channels
    let validChannels = ["work"]
    if (validChannels.includes(channel)) {
      ipcRenderer.send(channel, data)
    }
  },
  prefabInfo: () => ipcRenderer.invoke('dialog:prefabInfo'),
  picInfo: () => ipcRenderer.invoke('dialog:picInfo'),
  spineInfo: () => ipcRenderer.invoke('dialog:spineInfo'),
  scriptInfo: () => ipcRenderer.invoke('dialog:scriptInfo'),
})
