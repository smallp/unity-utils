import { app, BrowserWindow, ipcMain } from "electron"
import { join } from "path"
import * as eve from "./work"
function createWindow() {
  const win = new BrowserWindow({
    show: false,
    webPreferences: {
      nodeIntegration: false, // is default value after Electron v5
      contextIsolation: true, // protect against prototype pollution
      preload: join(__dirname, "preload.js"),
      webSecurity: false,
    },
  })
  win.maximize()
  if (process.env.NODE_ENV == "dev") {
    win.loadURL("http://localhost:5173/")
    win.webContents.openDevTools()
  } else {
    win.setMenu(null)
    win.loadFile("dist/index.html")
  }
  win.show()
}

function regEvents() {
  ipcMain.handle('dialog:picInfo', eve.picInfo)
  ipcMain.handle('dialog:prefabInfo', eve.prefabInfo)
  ipcMain.handle('dialog:spineInfo', eve.spineInfo)
  ipcMain.handle('dialog:scriptInfo', eve.scriptInfo)
  ipcMain.on("work", eve.work)
}

app.whenReady().then(() => {
  regEvents()
  createWindow()
  app.on("activate", function () {
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})
